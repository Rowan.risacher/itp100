from gasp import *
begin_graphics()
#head
Circle((300, 200), (75))
#eyes
Circle((265, 210), (10))
Circle((335, 210), (10))
#nose
Line((300, 210), (290, 190))
Line((290, 190), (310, 190))
#mouth
Arc((300, 200), 30, 225, 315)
update_when('key_pressed')
end_graphics()
