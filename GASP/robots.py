from gasp import *          # As usual

class Player:
    pass

class Robot:
    pass

begin_graphics()            # So that you can draw things
finished = False


def place_player(player):
    player.x = random_between(0, 63)
    player.y = random_between(0, 47)

def safely_place_player():
    global player
    global robot
    player = Player()
    place_player(player)
    while collided(player, robot):
        place_player(player)
    player.shape = Circle((10*player.x+5, 10*player.y+5), 5, filled=True)
    return player
    
def place_robot():
    global robot
    robot = Robot()
    robot.x = random_between(0, 63)
    robot.y = random_between(0, 47)
    robot.shape = Box((10*robot.x, 10*robot.y), 10, 10, filled=False)
    return robot
   
def move_player(player):
    global robot
    key = update_when('key_pressed')
    if key == 'j': 
        player.x -= 1
        player.y -= 1
    if key == 'k': 
        player.y -= 1
    if key == 'l': 
        player.x += 1
        player.y -= 1
    if key == 'u': 
        player.x -= 1
    if key == 'i': 
        # teleport 
        place_player(player)
        while collided(player, robot):
            place_player(player)
    if key == 'o': 
        player.x += 1
    if key == '7': 
        player.x -= 1
        player.y += 1
    if key == '8': 
        player.y += 1
    if key == '9': 
        player.x += 1
        player.y += 1
    if player.x > 63: # stay in the world
        player.x = 63
    if player.x < 0:
        player.x = 0
    if player.y > 47:
        player.y = 47
    if player.y < 0:
        player.y = 0
    print("I'm moving...")
    move_to(player.shape, (10*player.x+5, 10*player.y +5))

def move_robot(robot):
    global finished
    if robot.x > player.x:
        robot.x -= 1
    if robot.y > player.y:
        robot.y -= 1
    if robot.x < player.x:
        robot.x += 1
    if robot.y < player.y:
        robot.y += 1
    move_to(robot.shape, (10*robot.x, 10*robot.y))

def collided(player, robot): 
    return (robot.x == player.x and robot.y == player.y)
    
def check_collision():
    global player
    global robot
    global finished
    if collided(player, robot):
        Text("You have died. It's a sad day.", (240, 240))
        sleep(3)
        finished = True
    
        
place_robot()    
safely_place_player()

while not finished:
    move_player(player)
    move_robot(robot)
    check_collision()

end_graphics()              # Finished!

