def to_base(num, base):
    """
    Convert an decimal integer into a string representation of the digits
    representing the number in the base (between 2 and 16) provided.

      >>> to_base(10, 3)
      '101'
      >>> to_base(11, 2)
      '1011'
      >>> to_base(10, 6)
      '14'
      >>> to_base(21, 3)
      '210'
      >>> to_base(21, 11)
      '1A'
      >>> to_base(47, 16)
      '2F'
      >>> to_base(65535, 16)
      'FFFF'
      >>> to_base(254, 64)
      '3/'
    """
    digits = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz/+'
    if num == 0:
        return '0'
    numstr =''
    while num :
        numstr = digits[num % base] + numstr
        num //= base
    return numstr

def to_base_64(num, base):
    """
    Convert an decimal integer into a string representation of the digits
    representing the number in the base (between 2 and 16) provided.
     >>> to_base_64(254, 64)
     'D/'
     >>> to_base_64(11111, 64)
     'Ctn'
    """


    digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789/+'
    if num == 0:
        return '0'
    numstr =''
    while num :
        numstr = digits[num % base] + numstr
        num //= base
    return numstr

