def lots_of_letters(word):
    """
    >>> lots_of_letters('Lidia')
    'Liidddiiiiaaaaa'
    >>> lots_of_letters('Python')
    'Pyyttthhhhooooonnnnnn'
    >>> lots_of_letters('')
    ''
    >>> lots_of_letters('1')
    '1'
    """

    length = len(word)
    i = 0;
    res = ""
    while i < length:
        res += (word[i] * (i+1))
        i += 1
    return res

if __name__ == '__main__':
    import doctest
    doctest.testmod()
